# Wardrobify

Team:

* JUSTIN ISLES - Hats Microservice
* FRANCIS BELZA - Shoes Microservice

## Design

## Shoes microservice

The shoes microservice will pull objects from the wardrobe api using a poller. The microservice will also have a Shoe and BinVO models which will be used in our view functions enabling the ability to create, read, update, and delete shoes. As a side note, which some staff memebers were aware of, I've been having hardware issues since last week which hindered my learning ability. I had to cram the modules I missed two nights before this project was released in order to tackle it, but to no avail.

## Hats microservice

<!-- Explain your models and integration with the wardrobe
microservice, here. -->

- Install Django Hats app into Django project

- make simple model (don't worry about integration)

- make one function view to show list of model (ref Conf Go or Wardrobe API)

- Configure views in a URLs file for Django app

- include URLs from Django app into Django proj URLs
    (attempt to view in Insomnia)
    get code into main branch (ref Git In a Group)

- add function to handle POST req to create new record in database
    (check usability in Insomnia)
    (use GET to see if it's now in the list)
    get code into main branch (ref Git In a Group)

- build React component to fetch list and show list of hats in Web page
    (prettify as desired; React app already has Bootstrap in it)
    Route list component to correct path to show via navbar
        (ref AttendeesList or MainPage from Conf Go)
        But, you will make your own list component, like HatList
        get code into main branch (ref Git In a Group)

- keep adding functionality; push to Git
    keep going until you can use React UI to manipulate back-end:
        add
        view
        delete

- write poller to get Bin or Location data for microservice
    (use 'requests' library to make HTTP request to Wardrobe API;
    loop over it and add to database.)
    ref Account information from attendees microservice on Conf Go

- leave 'delete' functionality for last
    add delete button for each item in list;
    if give button an 'onClick' handler, can use to make fetch with HTTP method delete;
    figure out how to get the id of the thing you want to delete for the URL;
    <!-- e.g. <button onClick={() => this.delete(hat.id)}> -->
