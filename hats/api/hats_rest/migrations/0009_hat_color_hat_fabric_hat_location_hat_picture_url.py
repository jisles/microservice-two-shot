# Generated by Django 4.0.3 on 2023-07-19 22:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0008_alter_hat_options_remove_hat_color_remove_hat_fabric_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='hat',
            name='color',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AddField(
            model_name='hat',
            name='fabric',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AddField(
            model_name='hat',
            name='location',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AddField(
            model_name='hat',
            name='picture_url',
            field=models.URLField(blank=True, null=True),
        ),
    ]
