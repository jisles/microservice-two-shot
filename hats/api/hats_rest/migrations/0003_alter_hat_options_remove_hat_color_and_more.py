# Generated by Django 4.0.3 on 2023-07-19 20:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_hat_color_hat_location_hat_picture_url'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='hat',
            options={'ordering': ('style',)},
        ),
        migrations.RemoveField(
            model_name='hat',
            name='color',
        ),
        migrations.RemoveField(
            model_name='hat',
            name='location',
        ),
        migrations.RemoveField(
            model_name='hat',
            name='picture_url',
        ),
    ]
