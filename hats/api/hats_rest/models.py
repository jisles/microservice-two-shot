from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    location = models.CharField(max_length=200, null=True)


class Hat(models.Model):
    fabric = models.CharField(max_length=100, blank=True)
    style = models.CharField(max_length=100)
    color = models.CharField(max_length=100, blank=True)
    picture_url = models.URLField(null=True, blank=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE
    )

    def get_api_url(self):
        return reverse("api_hat", kwargs={"pk": self.pk})

    class Meta:
        ordering = (
            # "location"
            # "color",
            "style",
            )
