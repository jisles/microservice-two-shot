import React, { useEffect, useState } from 'react';

const log = (...items) => console.log(...items)

function AddHatForm(props) {
  const [fabric, setFabric] = useState('')
  const [style, setStyle] = useState('')
  const [color, setColor] = useState('')
  const [pictureUrl, setPictureUrl] = useState('')
  const [location, setLocation] = useState('')
  const [locations, setLocations] = useState([])

  const handleFabricChange = (event) => {
    const value = event.target.value;
    setFabric(value)
  }
  const handleStyleChange = (event) => {
    const value = event.target.value;
    setStyle(value)
  }
  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value)
  }
  const handlePictureUrlChange = (event) => {
    const value = event.target.value;
    setPictureUrl(value)
  }
  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value)
  }
  const fetchData = async () => {
    const url = "insert url for locations"
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {}
    data.fabric = fabric;
    data.style = style;
    data.color = color;
    data.pictureUrl = pictureUrl
    data.location = location

    const hatUrl = "http://localhost:8090/api/hats/" // not sure
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      log(newHat);
      setFabric('');
      setStyle('');
      setColor('');
      setPictureUrl('');
    }
  }

  return (
    <div>
      Hat.js div statement
    </div>
  )
}

export default AddHatForm
