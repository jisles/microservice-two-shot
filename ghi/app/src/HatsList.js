function HatsList(props) {
  return(
    <table className="table table-striped">
      <thead>
        <tr>
          <th>fabric</th>
          <th>style</th>
          <th>color</th>
          <th>picture url</th>
        </tr>
      </thead>
      <tbody>
        {props.hats && props.hats.map(hat => {
          return (
            <tr key={hat.href}>
              <td>{hat.fabric}</td>
              <td>{hat.style}</td>
              <td>{hat.color}</td>
              <td>{hat.picture_url}</td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}

export default HatsList
